from django.contrib.auth import get_user_model
User = get_user_model()
from django.contrib.auth.forms import UserCreationForm
from django import forms



class RegisterForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('email', 'phone_number', 'last_name', 'first_name')
